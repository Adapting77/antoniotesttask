# How to run

1) Setup .env and docker-compose files. Project have example files
2) Run - docker-compose up -d
3) Run - docker-compose exec php-fpm bash
4) Run - composer install
5) Run - d:d:c
6) Run - d:s:c
7) Load fixtures - d:f:l -n 
5) Generate public and private keys
6) mkdir -p config/jwt
7) openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
8) openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
9) Project would be available at 'http://127.0.0.1:8000'

# Project have example Unit and Behat tests
1) Configure behat.yaml. Project have examples
2) APP_ENV=test vendor/bin/behat
3) php bin/phpunit

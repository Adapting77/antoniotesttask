<?php

namespace App\Tests\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\BlogPost;
use App\Entity\User;
use App\EventSubscriber\BlogPostSubscriber;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

class BlogPostSubscriberTest extends TestCase
{
    public function testConfiguration()
    {
        $result = BlogPostSubscriber::getSubscribedEvents();
        $this->assertArrayHasKey(KernelEvents::VIEW, $result);
        $this->assertEquals(
            ['onBlogPostCreate', EventPriorities::PRE_WRITE],
            $result[KernelEvents::VIEW]
        );
    }

    /**
     * @dataProvider providerSetCreatedAtCall
     */
    public function testSetCreatedAtCall(
        string $className,
        bool $shouldCallSetAuthor,
        string $method,
        bool $shouldSetUserCall
    ) {
        $entityMock = $this->getEntityMock($className, $shouldCallSetAuthor);
        $securityMock = $this->getSecurityMock($shouldSetUserCall);
        $eventMock = $this->getEventMock($method, $entityMock);

        (new BlogPostSubscriber($securityMock))->onBlogPostCreate(
            $eventMock
        );
    }

    public function providerSetCreatedAtCall(): array
    {
        return [
            [BlogPost::class, true, 'POST', true],
            [BlogPost::class, false, 'GET', false],
            ['NonExisting', false, 'POST', false],
        ];
    }

    /**
     * @return MockObject|ViewEvent
     */
    private function getEventMock(string $method, $controllerResult): MockObject
    {
        $requestMock = $this->getMockBuilder(Request::class)
            ->getMock();
        $requestMock->expects($this->once())
            ->method('getMethod')
            ->willReturn($method);
        $eventMock =
            $this->getMockBuilder(ViewEvent::class)
                ->disableOriginalConstructor()
                ->getMock();
        $eventMock->expects($this->once())
            ->method('getControllerResult')
            ->willReturn($controllerResult);
        $eventMock->expects($this->once())
            ->method('getRequest')
            ->willReturn($requestMock);
        return $eventMock;
    }

    /**
     * @return MockObject
     */
    private function getEntityMock(string $className, bool $shouldCallSetAuthor): MockObject
    {
        $entityMock = $this->getMockBuilder($className)
            ->setMethods(['setCreatedAt'])
            ->getMock();
        $entityMock->expects($shouldCallSetAuthor ? $this->once() : $this->never())
            ->method('setCreatedAt');
        return $entityMock;
    }

    /**
     * @return MockObject|Security
     */
    private function getSecurityMock(bool $shouldGetUserCall): MockObject
    {
        $securityMock = $this->getMockBuilder(Security::class)
            ->disableOriginalConstructor()
            ->setMethods(['getUser'])
            ->getMockForAbstractClass();
        $securityMock->expects($shouldGetUserCall ? $this->once() : $this->never())
            ->method('getUser')
            ->willReturn(new User());

        return $securityMock;
    }
}
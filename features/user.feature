Feature: Manage users
  Scenario: Admin access to users list
    Given I am authenticated as "admin@admin.com" with password "admin"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"
    And I send a "GET" request to "/api/users"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON nodes should contain:
      | @context             | /api/contexts/User       |
      | @id                  | /api/users               |
      | @type                | hydra:Collection         |
      | hydra:totalItems     |                          |

  Scenario: Not accepted user try to create post
    Given I am authenticated as "user@user.com" with password "user"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"
    And I send a "GET" request to "/api/users"
    Then the response status code should be 403
    And the JSON nodes should contain:
      | hydra:description     | Access Denied.          |

  Scenario: User can see only his own profile
    Given I am authenticated as "user@user.com" with password "user"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"
    And I send a "GET" request to "/api/users/2"
    Then the response status code should be 200
    And the JSON nodes should contain:
      | @context            | /api/contexts/User          |
      | @id                 | /api/users/2                |
      | @type               | User                        |
      | email               |                             |
      | firstName           |                             |
      | lastName            |                             |

  Scenario: User can't see other user profiles
    Given I am authenticated as "user@user.com" with password "user"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"
    And I send a "GET" request to "/api/users/4"
    Then the response status code should be 403
    And the JSON nodes should contain:
      | hydra:description     | Access Denied.          |

  Scenario: Admin can see any use profile
    Given I am authenticated as "admin@admin.com" with password "admin"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"
    And I send a "GET" request to "/api/users/6"
    Then the response status code should be 200
    And the JSON nodes should contain:
      | @context            | /api/contexts/User          |
      | @id                 | /api/users/6                |
      | @type               | User                        |
      | email               |                             |
      | firstName           |                             |
      | lastName            |                             |

  Scenario: User can update his profile
    Given I am authenticated as "user@user.com" with password "user"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"
    And I send a "PUT" request to "/api/users/2" with body:
    """
    {
      "firstName": "max"
    }
    """
    Then the response status code should be 200
    And the JSON nodes should contain:
      | @context            | /api/contexts/User          |
      | @id                 | /api/users/2                |
      | @type               | User                        |
      | email               | user@user.com               |
      | firstName           | max                         |
      | lastName            |                             |

  Scenario: User can't edit other profiles
    Given I am authenticated as "user@user.com" with password "user"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"
    And I send a "PUT" request to "/api/users/4" with body:
    """
    {
      "firstName": "Oleg"
    }
    """
    Then the response status code should be 403
    And the JSON nodes should contain:
      | hydra:description     | Access Denied.           |

  Scenario: Admin can edit any profile
    Given I am authenticated as "admin@admin.com" with password "admin"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"
    And I send a "PUT" request to "/api/users/8" with body:
    """
    {
      "firstName": "Oleg"
    }
    """
    Then the response status code should be 200
    And the JSON nodes should contain:
      | @context            | /api/contexts/User          |
      | @id                 | /api/users/8                |
      | @type               | User                        |
      | email               |                             |
      | firstName           | Oleg                        |
      | lastName            |                             |


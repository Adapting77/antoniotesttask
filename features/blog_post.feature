Feature: Manage blog posts
  Scenario: Create a blog post without authentication
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"
    And I send a "POST" request to "/api/blog_posts" with body:
    """
    {
      "title": "Hello a title",
      "content": "The content is suppose to be at least 20 characters",
    }
    """
    Then the response status code should be 401
    And the response should be in JSON
    And the JSON nodes should contain:
      | code                    | 401                     |
      | message                 | JWT Token not found     |

  Scenario: Admin create blog post
    Given I am authenticated as "admin@admin.com" with password "admin"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"
    And I send a "POST" request to "/api/blog_posts" with body:
    """
    {
      "title": "Hello a title",
      "content": "Unbelievable content"
    }
    """
    Then the response status code should be 201
    And the JSON nodes should contain:
      | @context            | /api/contexts/BlogPost      |
      | @id                 |                             |
      | @type               |                             |
      | title               | Hello a title               |
      | content             | Unbelievable content        |

  Scenario: Accepted user create post
    Given I am authenticated as "user@user.com" with password "user"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"
    And I send a "POST" request to "/api/blog_posts" with body:
    """
    {
      "title": "Hello a title",
      "content": "Unbelievable content"
    }
    """
    Then the response status code should be 201
    And the JSON nodes should contain:
      | @context            | /api/contexts/BlogPost      |
      | @id                 |                             |
      | @type               |                             |
      | title               | Hello a title               |
      | content             | Unbelievable content        |

  Scenario: User posts watch
    Given I am authenticated as "user@user.com" with password "user"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"
    And I send a "GET" request to "/api/blog_posts"
    Then the response status code should be 200
    And the JSON nodes should contain:
      | @type                | hydra:Collection         |
      | hydra:totalItems     |                          |

  Scenario: Admin get individual item access
    Given I am authenticated as "admin@admin.com" with password "admin"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"
    And I send a "GET" request to "/api/blog_posts/1"
    Then the response status code should be 200
    And the JSON nodes should contain:
      | @context            | /api/contexts/BlogPost      |
      | @id                 | /api/blog_posts/1           |
      | @type               | BlogPost                    |
      | title               |                             |
      | content             |                             |
      | createdAt           |                             |
      | user                |                             |

  Scenario: Admin can access every item
    Given I am authenticated as "admin@admin.com" with password "admin"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"
    And I send a "PUT" request to "/api/blog_posts/1" with body:
    """
    {
      "title": "Changed title",
      "content": "New title"
    }
    """
    Then the response status code should be 200
    And the JSON nodes should contain:
      | @context            | /api/contexts/BlogPost      |
      | @id                 | /api/blog_posts/1           |
      | @type               | BlogPost                    |
      | title               |                             |
      | content             |                             |
      | createdAt           |                             |
<?php

use Behat\Gherkin\Node\PyStringNode;

class FeatureContext extends \Behatch\Context\RestContext
{
    const AUTHENTICATION_PATH = '/api/authentication_token';

    const AUTH_JSON = '
        {
            "email": "%s",
            "password": "%s"
        }
    ';
    private $matcher;

    public function __construct(
        \Behatch\HttpCall\Request $request
    ) {
        parent::__construct($request);
        $this->matcher = (new \Coduo\PHPMatcher\Factory\SimpleFactory())->createMatcher();
    }

    /**
     * @Given I am authenticated as :username with password :password
     */
    public function iAmAuthenticatedAs($username, $password)
    {
        $this->request->setHttpHeader('Content-Type', 'application/ld+json');
        $this->request->send(
            'POST',
            $this->locatePath(self::AUTHENTICATION_PATH),
            [],
            [],
            sprintf(self::AUTH_JSON, $username, $password)
        );
        $json = json_decode($this->request->getContent(), true);
        // Make sure the token was returned
        $this->assertTrue(isset($json['token']));
        $token = $json['token'];
        $this->request->setHttpHeader(
            'Authorization',
            'Bearer '.$token
        );
    }
}

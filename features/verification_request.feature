Feature: Manage verification requests
  Scenario: Admin can retrieve verification requests collection
    Given I am authenticated as "admin@admin.com" with password "admin"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"
    And I send a "GET" request to "/api/verification_requests"
    Then the response status code should be 200
    And the JSON nodes should contain:
      | @type                | hydra:Collection         |
      | hydra:totalItems     |                          |

  Scenario: Users can't retrieve verification requests collection
    Given I am authenticated as "user@user.com" with password "user"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"
    And I send a "GET" request to "/api/verification_requests"
    Then the response status code should be 403
    And the JSON nodes should contain:
      | hydra:description     | Access Denied.          |

  Scenario: Users can retrieve their own verification request
    Given I am authenticated as "user@user.com" with password "user"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"
    And I send a "GET" request to "/api/verification_requests/1"
    Then the response status code should be 200
    And the JSON nodes should contain:
      | @context            | /api/contexts/VerificationRequest  |
      | @id                 | /api/verification_requests/1       |
      | @type               | VerificationRequest                |
      | imagePath           |                                    |
      | message             |                                    |
      | status              |  APPROVED                          |

  Scenario: Users can't retrieve other verification requests
    Given I am authenticated as "user@user.com" with password "user"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"
    And I send a "GET" request to "/api/verification_requests/4"
    Then the response status code should be 403
    And the JSON nodes should contain:
      | hydra:description     | Access Denied.          |

  Scenario: Users can't edit their verification request which have status APPROVED or DECLINED
    Given I am authenticated as "user@user.com" with password "user"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"
    And I send a "PUT" request to "/api/verification_requests/1"
    Then the response status code should be 403
    And the JSON nodes should contain:
      | hydra:description     | Access Denied.          |

  Scenario: Admin can edit field like verification status, and rejection reason of every verification post
    Given I am authenticated as "admin@admin.com" with password "admin"
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"
    And I send a "PUT" request to "/api/verification_requests/10" with body:
    """
    {
      "status": "DECLINED",
      "rejectionReason": "Super interesting reason"
    }
    """
    Then the response status code should be 200
    And the JSON nodes should contain:
      | @context            | /api/contexts/VerificationRequest  |
      | @id                 | /api/verification_requests/10      |
      | @type               | VerificationRequest                |
      | status              | DECLINED                           |
      | rejectionReason     | Super interesting reason           |
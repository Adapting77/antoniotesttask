Feature: Manage blog posts
  Scenario: Authenticate as regular user
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"
    And I send a "POST" request to "/api/authentication_token" with body:
    """
    {
        "email": "user@user.com",
        "password": "user"
    }
    """
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON nodes should contain:
      | token                   |                      |

  Scenario: Authenticate as admin
    When I add "Content-Type" header equal to "application/ld+json"
    And I add "Accept" header equal to "application/ld+json"
    And I send a "POST" request to "/api/authentication_token" with body:
    """
    {
        "email": "user@user.com",
        "password": "user"
    }
    """
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON nodes should contain:
      | token                   |                      |
<?php

namespace App\DataFixtures;

use App\Entity\VerificationRequest;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class VerificationRequestFixtures extends BaseFixture implements DependentFixtureInterface
{
    const VERIFICATION_STATUSES = [
        VerificationRequest::APPROVED,
        VerificationRequest::DECLINED,
        VerificationRequest::VERIFICATION_REQUESTED
    ];
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(20, 'main_verification_requests', function ($i) {
        $verificationRequest = new VerificationRequest();
        $verificationRequest->setImagePath('public/uploads');
        $verificationRequest->setMessage($this->faker->realText(20));
        $verificationRequest->setStatus($this->faker->randomElement(self::VERIFICATION_STATUSES));
        $verificationRequest->setUser($this->getReference('main_users_' . $i));
        $verificationRequest->setCreatedAt($this->faker->dateTimeBetween('- 5 days', 'now'));

        return $verificationRequest;
    });
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }
}
<?php

namespace App\DataFixtures;

use App\Entity\BlogPost;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class BlogPostFixtures extends BaseFixture implements DependentFixtureInterface
{
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(50, 'main_blog_posts', function () {
            $blogPost = new BlogPost();
            $blogPost->setTitle($this->faker->realText(40));
            $blogPost->setContent($this->faker->realText(100));
            $blogPost->setCreatedAt($this->faker->dateTimeBetween('- 4 days', 'now'));
            $blogPost->setUser($this->getRandomReference('main_users'));

            return $blogPost;
        });
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }
}
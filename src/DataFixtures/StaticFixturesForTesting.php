<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\VerificationRequest;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class StaticFixturesForTesting extends BaseFixture
{
    private $passwordEncoder;
    const STATIC_ACCOUNT = [
        'admin' => ['admin@admin.com', 'admin'],
        'user' => ['user@user.com', 'user']
    ];

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(1, 'main_admin', function () {
            $user = new User();
            $user->setEmail(self::STATIC_ACCOUNT['admin'][0]);
            $user->setFirstName($this->faker->firstName);
            $user->setLastName($this->faker->lastName);
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                self::STATIC_ACCOUNT['admin'][1]
            ));
            $user->setRoles([User::ROLE_ADMIN]);

            return $user;
        });

        $this->createMany(1, 'main_user', function () {
            $user = new User();
            $user->setEmail(self::STATIC_ACCOUNT['user'][0]);
            $user->setFirstName($this->faker->firstName);
            $user->setLastName($this->faker->lastName);
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                self::STATIC_ACCOUNT['user'][1]
            ));

            return $user;
        });
        $manager->flush();

        // Need relation to test this cases
        $this->createMany(1, 'main_verification_request', function () {
            $verificationRequest = new VerificationRequest();
            $verificationRequest->setImagePath('public/uploads');
            $verificationRequest->setMessage($this->faker->realText(20));
            $verificationRequest->setStatus(VerificationRequest::APPROVED);
            $verificationRequest->setUser($this->getRandomReference('main_user'));
            $verificationRequest->setCreatedAt($this->faker->dateTimeBetween('- 5 days', 'now'));

            return $verificationRequest;
        });
        $manager->flush();
    }
}
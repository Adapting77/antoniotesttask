<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use App\Entity\VerificationRequest;
use App\Service\Mailer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class VerificationRequestSubscriber implements EventSubscriberInterface
{
    private $tokenStorage;
    private $authorizationChecker;
    private $mailer;

    public function __construct(
        TokenStorageInterface $tokenStorage,
        AuthorizationCheckerInterface $authorizationChecker,
        Mailer $mailer
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
        $this->mailer = $mailer;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['onVerificationRequest', EventPriorities::PRE_WRITE]
        ];
    }

    public function onVerificationRequest(ViewEvent $event): void
    {
        $verificationRequest = $event->getControllerResult();
        $method = $event->getRequest()
            ->getMethod();
        // Handle admin changing status to approved or decliner
        if (!in_array($method, [Request::METHOD_PUT, Request::METHOD_PATCH])
            || !$this->authorizationChecker->isGranted(User::ROLE_ADMIN)
            || !$verificationRequest instanceof VerificationRequest
            || $verificationRequest->getStatus() !== VerificationRequest::VERIFICATION_REQUESTED) {
            return;
        }

        $this->mailer->sendValidationRequestNotify($verificationRequest);
    }
}

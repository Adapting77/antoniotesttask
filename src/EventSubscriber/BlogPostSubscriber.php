<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\BlogPost;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

class BlogPostSubscriber implements EventSubscriberInterface
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['onBlogPostCreate', EventPriorities::PRE_WRITE]
        ];
    }

    public function onBlogPostCreate(ViewEvent $event): void
    {
        $blogPost = $event->getControllerResult();
        $method = $event->getRequest()
            ->getMethod();

        if (!$blogPost instanceof BlogPost ||
            !in_array($method, [Request::METHOD_POST])) {
            return;
        }
        $blogPost->setCreatedAt(new \DateTime('now'));
        $blogPost->setUser($this->security->getUser());
    }
}
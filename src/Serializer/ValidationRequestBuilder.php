<?php

namespace App\Serializer;

use ApiPlatform\Core\Serializer\SerializerContextBuilderInterface;
use App\Entity\User;
use App\Entity\VerificationRequest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

final class ValidationRequestBuilder implements SerializerContextBuilderInterface
{

    private $authorizationChecker;
    private $decorated;

    public function __construct(
        SerializerContextBuilderInterface $decorated,
        AuthorizationCheckerInterface $authorizationChecker
    ) {

        $this->authorizationChecker = $authorizationChecker;
        $this->decorated = $decorated;
    }

    public function createFromRequest(
        Request $request,
        bool $normalization,
        array $extractedAttributes = null
    ): array {

        $context = $this->decorated->createFromRequest($request, $normalization, $extractedAttributes);
        $resourceClass = $context['resource_class'] ?? null;

        if ($resourceClass === VerificationRequest::class
            && isset($context['groups'])
            && $this->authorizationChecker->isGranted(User::ROLE_ADMIN)
            && false === $normalization
        ) {
            $context['groups'][] = 'admin:input';
        }

        return $context;
    }
}
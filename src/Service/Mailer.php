<?php

namespace App\Service;

use App\Entity\User;
use App\Entity\VerificationRequest;
use Swift_Message;
use Twig\Environment;

class Mailer
{
    private $mailer;
    private $twigEnvironment;

    public function __construct(\Swift_Mailer $mailer, Environment $twigEnvironment)
    {
        $this->mailer = $mailer;
        $this->twigEnvironment = $twigEnvironment;
    }

    public function sendValidationRequestNotify(VerificationRequest $verificationRequest)
    {
        $message = '';
        if ($verificationRequest->getStatus() == VerificationRequest::APPROVED) {
            $message = 'Your account is approved. Now you can post something';
            $verificationRequest->getUser()->setRoles([User::ROLE_BLOGGER]);
        } elseif ($verificationRequest->getStatus() === VerificationRequest::DECLINED) {
            $message = 'Your account is declined';
            if ($verificationRequest->getRejectionReason()) {
                $message .= 'The reason is' . $verificationRequest->getRejectionReason();
            }
        }

        $body = $this->twigEnvironment->render(
            'email/verification_request.html.twig',
            [
                'message' => $message
            ]
        );

        $message = (new Swift_Message($message))
            ->setFrom('api-platform@api.com')
            ->setTo($verificationRequest->getUser()->getEmail())
            ->setBody($body, 'text/html');

        $this->mailer->send($message);
    }
}
<?php

namespace App\Security\Voter;

use App\Entity\VerificationRequest;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class VerificationRequestVoter extends Voter
{
    protected function supports($attribute, $subject): bool
    {
        return in_array($attribute, ['VERIFICATION_REQUEST_EDIT'])
            && $subject instanceof VerificationRequest;
    }

    /**
     * @param string $attribute
     * @param VerificationRequest $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof UserInterface) {
            return false;
        }

        switch ($attribute) {
            case 'VERIFICATION_REQUEST_EDIT':
                if ($subject->getStatus() === VerificationRequest::VERIFICATION_REQUESTED
                    && $subject->getUser() === $user) {
                    return true;
                }
                break;
        }

        return false;
    }
}

<?php

namespace App\Security\Voter;

use App\Entity\BlogPost;
use App\Entity\User;
use App\Entity\VerificationRequest;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class BlogPostVoter extends Voter
{
    protected function supports($attribute, $subject)
    {
        return in_array($attribute, ['POST_UPDATE'])
            && $subject instanceof BlogPost
            || in_array($attribute, ['POST_CREATE']);
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /** @var User $user */
        $user = $token->getUser();

        if (!$user instanceof UserInterface) {
            return false;
        }
        switch ($attribute) {
            case 'POST_CREATE':
                if (null !== $user->getVerificationRequest()
                    && $user->getVerificationRequest()->getStatus() === VerificationRequest::APPROVED) {
                    return true;
                }
                break;
            case 'POST_UPDATE':
                if (null !== $user->getVerificationRequest()
                    && $user->getVerificationRequest()->getStatus() === VerificationRequest::APPROVED
                    && $subject->getUser() === $user) {
                    return true;
                }
                break;
        }

        return false;
    }
}

<?php

namespace App\Controller\ApiPlatform;

use ApiPlatform\Core\Validator\ValidatorInterface;
use ApiPlatform\Core\Validator\Exception\ValidationException;
use App\Entity\User;
use App\Entity\VerificationRequest;
use App\Form\VerificationRequestType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class VerificationRequestAction
{
    private $formFactory;
    private $validator;
    private $entityManager;
    private $security;

    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        Security $security
    ) {
        $this->formFactory = $formFactory;
        $this->validator = $validator;
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    public function __invoke(Request $request)
    {
        $verificationRequest = new VerificationRequest();
        // Create and handle form
        $form = $this->formFactory->create(VerificationRequestType::class, $verificationRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Set default status, current user and CreatedAt
            $verificationRequest->setStatus(VerificationRequest::VERIFICATION_REQUESTED);
            /** @var User $user */
            $user = $this->security->getUser();
            $verificationRequest->setUser($user);
            $verificationRequest->setCreatedAt(new \DateTime('now'));

            // Uploading and moving file done by Vich Uploader
            $this->entityManager->persist($verificationRequest);
            $this->entityManager->flush();
            // Dont need to return file
            $verificationRequest->setImageFile(null);

            return $verificationRequest;
        }

        // This exception means that something went wrong with form validation
        throw new ValidationException(
            $this->validator->validate($verificationRequest)
        );
    }
}
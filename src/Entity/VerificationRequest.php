<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\ApiPlatform\VerificationRequestAction;

/**
 * @ApiResource(
 *     attributes={
 *         "force_eager"=false,
 *     },
 *     collectionOperations={
 *         "get"={
 *             "security"="is_granted('ROLE_ADMIN')",
 *             "normalization_context"={
 *                 "groups"={"admin:output"}
 *             }
 *         },
 *         "post"={
 *             "security"="is_granted('ROLE_USER')",
 *             "path"="/verification_requests",
 *             "controller"=VerificationRequestAction::class,
 *             "defaults"={"_api_receive"=false}
 *         }
 *     },
 *     itemOperations={
 *         "get"={
 *             "security"="(object.getUser() == user) or is_granted('ROLE_ADMIN')",
 *             "normalization_context"={
 *                 "groups"={"user:read"}
 *             }
 *         },
 *         "put"={
 *             "security"="is_granted('VERIFICATION_REQUEST_EDIT', object) or is_granted('ROLE_ADMIN')",
 *             "normalization_context"={
 *                 "groups"={"user:read"}
 *             },
 *             "denormalization_context"={
 *                 "groups"={"user:write"}
 *             }
 *         },
 *         "patch"={
 *             "security"="is_granted('VERIFICATION_REQUEST_EDIT', object) or is_granted('ROLE_ADMIN')",
 *             "normalization_context"={
 *                 "groups"={"user:read"}
 *             },
 *             "denormalization_context"={
 *                 "groups"={"user:write"}
 *             }
 *         },
 *         "delete"={"security"="is_granted('ROLE_ADMIN')"}
 *     }
 * )
 * @ApiFilter(OrderFilter::class, properties={"createdAt"}, arguments={"orderParameterName"="order"})
 * @ApiFilter(SearchFilter::class, properties={
 *     "status": "exact",
 *     "user.firstName": "exact",
 *     "user.lastName": "exact",
 *     "user.email": "exact"
 * })
 * @ORM\Entity(repositoryClass="App\Repository\VerificationRequestRepository")
 * @Vich\Uploadable()
 */
class VerificationRequest
{
    const VERIFICATION_REQUESTED = 'VERIFICATION_REQUESTED';
    const APPROVED = 'APPROVED';
    const DECLINED = 'DECLINED';
    const DOWNLOAD_DIR = '/uploads/verification_images/';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Vich\UploadableField(mapping="verification_images", fileNameProperty="imagePath")
     * @Assert\NotBlank(groups={"user:write"})
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"admin:output", "admin:input", "user:read"})
     */
    private $imagePath;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"admin:output", "admin:input", "user:write", "user:read"})
     */
    private $message;

    /**
     * @ORM\Column(type="string", length=45)
     * @Groups({"admin:output", "admin:input", "user:read"})
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"admin:output", "admin:input", "user:read"})
     */
    private $rejectionReason;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="verificationRequest", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"admin:output", "admin:input"})
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"admin:output", "admin:input"})
     */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImageFile($imageFile): void
    {
        $this->imageFile = $imageFile;

        /** Set createdAt only exist in this place, there is no setter for this property */
        if (null !== $imageFile) {
            $this->createdAt = new \DateTime('now');
        }
    }

    public function getImagePath(): ?string
    {
        return self::DOWNLOAD_DIR . $this->imagePath;
    }

    public function setImagePath(string $imagePath): self
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getRejectionReason(): ?string
    {
        return $this->rejectionReason;
    }

    public function setRejectionReason(?string $rejectionReason): self
    {
        $this->rejectionReason = $rejectionReason;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}

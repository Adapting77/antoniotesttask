<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get",
 *         "post"={
 *             "security"="is_granted('POST_CREATE', object) or is_granted('ROLE_ADMIN')",
 *             "normalization_context"={
 *                 "groups"={"user:read"}
 *             },
 *             "denormalization_context"={
 *                 "groups"={"user:write"}
 *             }
 *         }
 *     },
 *     itemOperations={
 *         "get"={"security"="(object.getUser() == user)or is_granted('ROLE_ADMIN')"},
 *         "put"={
 *             "security"="is_granted('POST_UPDATE', object) or is_granted('ROLE_ADMIN')",
 *             "normalization_context"={
 *                 "groups"={"user:read"}
 *             },
 *             "denormalization_context"={
 *                 "groups"={"user:write"}
 *             }
 *         },
 *         "patch"={
 *             "security"="is_granted('POST_UPDATE', object) or is_granted('ROLE_ADMIN')",
 *             "normalization_context"={
 *                 "groups"={"user:read"}
 *             },
 *             "denormalization_context"={
 *                 "groups"={"user:write"}
 *             }
 *         },
 *         "delete"={"security"="is_granted('POST_UPDATE', object) or is_granted('ROLE_ADMIN')"}
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\BlogPostRepository")
 */
class BlogPost
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user:read", "user:write"})
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Groups({"user:read", "user:write"})
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"user:read"})
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="blogPosts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}

<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ApiResource(
 *     attributes={
 *         "force_eager"=false,
 *     },
 *     collectionOperations={
 *         "get"={
 *             "security"="is_granted('ROLE_ADMIN')",
 *             "normalization_context"={
 *                 "groups"={"admin:output"}
 *             }
 *         },
 *         "post"
 *     },
 *     itemOperations={
 *         "get"={
 *             "security"="(object == user) or is_granted('ROLE_ADMIN')",
 *             "normalization_context"={
 *                 "groups"={"user:read"}
 *             }
 *         },
 *         "put"={
 *             "security"="(object == user) or is_granted('ROLE_ADMIN')",
 *             "normalization_context"={
 *                 "groups"={"user:read"}
 *             },
 *             "denormalization_context"={
 *                 "groups"={"user:write"}
 *             }
 *         },
 *         "patch"={
 *             "security"="(object == user) or is_granted('ROLE_ADMIN')",
 *             "normalization_context"={
 *                 "groups"={"user:read"}
 *             },
 *             "denormalization_context"={
 *                 "groups"={"user:write"}
 *             }
 *         },
 *         "delete"={"security"="is_granted('ROLE_ADMIN')"}
 *     }
 * )
 */
class User implements UserInterface
{
    const ROLE_USER = 'ROLE_USER';
    const ROLE_BLOGGER = 'ROLE_BLOGGER';
    const ROLE_ADMIN = 'ROLE_ADMIN';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"user:write", "user:read", "admin:output"})
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     * @Groups({"admin:output"})
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Groups({"user:write", "admin:output"})
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Groups({"user:write", "user:read", "admin:output"})
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Groups({"user:write", "user:read", "admin:output"})
     */
    private $lastName;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BlogPost", mappedBy="user", orphanRemoval=true)
     * @Groups({"user:read", "admin:output"})
     */
    private $blogPosts;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\VerificationRequest", mappedBy="user", cascade={"persist", "remove"})
     * @Groups({"user:read", "admin:output"})
     */
    private $verificationRequest;

    public function __construct()
    {
        $this->blogPosts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return Collection|BlogPost[]
     */
    public function getBlogPosts(): Collection
    {
        return $this->blogPosts;
    }

    public function addBlogPost(BlogPost $blogPost): self
    {
        if (!$this->blogPosts->contains($blogPost)) {
            $this->blogPosts[] = $blogPost;
            $blogPost->setUser($this);
        }

        return $this;
    }

    public function removeBlogPost(BlogPost $blogPost): self
    {
        if ($this->blogPosts->contains($blogPost)) {
            $this->blogPosts->removeElement($blogPost);
            // set the owning side to null (unless already changed)
            if ($blogPost->getUser() === $this) {
                $blogPost->setUser(null);
            }
        }

        return $this;
    }

    public function getVerificationRequest(): ?VerificationRequest
    {
        return $this->verificationRequest;
    }

    public function setVerificationRequest(VerificationRequest $verificationRequest): self
    {
        $this->verificationRequest = $verificationRequest;

        // set the owning side of the relation if necessary
        if ($verificationRequest->getUser() !== $this) {
            $verificationRequest->setUser($this);
        }

        return $this;
    }
}
